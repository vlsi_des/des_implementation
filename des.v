module des_encrypt(input wire clk, input wire enable, input wire [63:0] plaintext, input wire [63:0] key, output reg [63:0] ciphertext);
    reg [63:0] temp;
    reg [31:0] L[0:16], R[0:16];
    reg [47:0] subkey[0:16];
    reg [55:0] temp_subkey[0:16];
    integer index;

    `include "helper_functions.v"

    // Get each subkey
    always @(key) begin
        if (enable) begin
            temp_subkey[0] = PC1_permutation(key);
            for (index=1; index<=16; index=index+1)
            begin
                temp_subkey[index] = shift_key(temp_subkey[index-1], index);
                subkey[index] = PC2_permutation(temp_subkey[index]);
            end
        end
        else begin
            temp_subkey[0] = 0;
            for (index=1; index<=16; index=index+1)
            begin
                temp_subkey[index] = 0;
                subkey[index] = 0;
            end
        end
    end

    always @(posedge clk) begin
        if (!enable) begin
            temp = 0;
            {L[0], R[0]} <= 0;
            for(index=1; index<=16; index=index+1)
            begin
                L[index] <= 0;
                R[index] <= 0;
            end

            ciphertext[63:0] <= 0;
        end
        else begin
            temp = initial_permutation(plaintext);

            // 16 rounds
            {L[0], R[0]} <= temp;
            for(index=1; index<=16; index=index+1)
            begin
                L[index] <= R[index-1];
                R[index] <= L[index-1] ^ feistel(R[index-1], subkey[index]);
            end

            ciphertext[63:0] <= final_permutation({R[16], L[16]});
        end
    end


endmodule //des_encrypt

module des_decrypt(input wire clk, input wire enable, input wire [63:0] ciphertext, input wire [63:0] key, output reg [63:0] plaintext);
    reg [63:0] temp;
    reg [31:0] L[0:16], R[0:16];
    reg [47:0] subkey[1:16];
    reg [55:0] temp_subkey[0:16];
    integer index;

    `include "helper_functions.v"

    // Get each subkey
    always @(key) begin
        if (enable) begin
            temp_subkey[0] = PC1_permutation(key);
            for (index=1; index<=16; index=index+1)
            begin
                temp_subkey[index] = shift_key(temp_subkey[index-1], index);
                subkey[17-index] = PC2_permutation(temp_subkey[index]);
            end
        end
        else begin
            temp_subkey[0] = 0;
            for (index=1; index<=16; index=index+1)
            begin
                temp_subkey[index] = 0;
                subkey[17-index] = 0;
            end
        end
    end

    always @(posedge clk) begin
        if (enable) begin
            // IP
            temp = initial_permutation(ciphertext);

            // 16 rounds
            {L[0], R[0]} <= temp;
            for(index=1; index<=16; index=index+1)
            begin
                L[index] <= R[index-1];
                R[index] <= L[index-1] ^ feistel(R[index-1], subkey[index]);
            end

            // FP (R and L need to be swapped for final permutation)
            plaintext[63:0] <= final_permutation({R[16], L[16]});
        end
        else begin
            temp = 0;
            {L[0], R[0]} <= 0;
            for(index=1; index<=16; index=index+1)
            begin
                L[index] <= 0;
                R[index] <= 0;
            end

            plaintext[63:0] <= 0;
        end
    end

endmodule //des_decrypt
