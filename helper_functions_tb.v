// This only tests a subset of the input and output of each helper function.
// If the encryption fails, more tests may be needed.
// Use test_SBOX module style if more tests are needed.

module test_initial_permutation;
    wire[64:1] in1;
    wire[64:1] in2;
    wire[64:1] out1;
    wire[64:1] out2;
    integer i;

    `include "helper_functions.v"

    // Input message of 5555555555555555
    // Output message of ffffffff00000000
    assign in1 = 64'h5555555555555555;
    assign out1 = initial_permutation(in1);

    initial
    begin
        if(out1 !== 64'hffffffff00000000)
        begin
            $display("Output message failed on ffffffff00000000");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Input message of 00000000ffffffff
    // Output message of f0f0f0f0f0f0f0f0
    assign in2 = 64'h00000000ffffffff;
    assign out2 = initial_permutation(in2);

    initial
    begin
        if(out2 !== 64'hf0f0f0f0f0f0f0f0)
        begin
            $display("Output message failed on f0f0f0f0f0f0f0f0");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_initial_permutation;

module test_final_permutation;
    wire[64:1] in1;
    wire[64:1] in2;
    wire[64:1] out1;
    wire[64:1] out2;
    integer i;

    `include "helper_functions.v"

    // Input message of 5555555555555555
    // Output message of ff00ff00ff00ff00
    assign in1 = 64'h5555555555555555;
    assign out1 = final_permutation(in1);

    initial
    begin
        if(out1 !== 64'hff00ff00ff00ff00)
        begin
            $display("Output message failed on ff00ff00ff00ff00");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Input message of 00000000ffffffff
    // Output message of aaaaaaaaaaaaaaaa
    assign in2 = 64'h00000000ffffffff;
    assign out2 = final_permutation(in2);

    initial
    begin
        if(out2 !== 64'haaaaaaaaaaaaaaaa)
        begin
            $display("Output message failed on aaaaaaaaaaaaaaaa");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_final_permutation;

module test_expansion;
    wire[32:1] in1;
    wire[32:1] in2;
    wire[48:1] out1;
    wire[48:1] out2;
    integer i;

    `include "helper_functions.v"

    // Input message of 5555555
    // Output message of aaaaaaaaaaaa
    assign in1 = 32'h55555555;
    assign out1 = expansion(in1);

    initial
    begin
        if(out1 !== 48'haaaaaaaaaaaa)
        begin
            $display("Output message failed on aaaaaaaaaaaa");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Input message of 0000ffff
    // Output message of 800017ffffe
    assign in2 = 32'h0000ffff;
    assign out2 = expansion(in2);

    initial
    begin
        if(out2 !== 48'h8000017ffffe)
        begin
            $display("Output message failed on 8000017ffffe");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_expansion;

module test_permutation;
    wire[32:1] in1;
    wire[32:1] in2;
    wire[32:1] out1;
    wire[32:1] out2;
    integer i;

    `include "helper_functions.v"

    // Input message of 55555555
    // Output message of a615f83a
    assign in1 = 32'h55555555;
    assign out1 = permutation(in1);

    initial
    begin
        if(out1 !== 32'ha615f83a)
        begin
            $display("Output message failed on a615f83a");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Input message of 0000ffff
    // Output message of 3b362ca9
    assign in2 = 32'h0000ffff;
    assign out2 = permutation(in2);

    initial
    begin
        if(out2 !== 32'h3b362ca9)
        begin
            $display("Output message failed on 3b362ca9");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_permutation;

module test_PC1_permutation;
    wire[64:1] in1;
    wire[64:1] in2;
    wire[56:1] out1;
    wire[56:1] out2;
    integer i;

    `include "helper_functions.v"

    // Input message of 5555555555555555
    // Output message of 00ff00f00ff00f
    assign in1 = 64'h5555555555555555;
    assign out1 = PC1_permutation(in1);

    initial
    begin
        if(out1 !== 56'h00ff00f00ff00f)
        begin
            $display("Output message failed on 00ff00f00ff00f");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Input message of 00000000ffffffff
    // Output message of f0f0f0ff0f0f00
    assign in2 = 64'h00000000ffffffff;
    assign out2 = PC1_permutation(in2);

    initial
    begin
        if(out2 !== 56'hf0f0f0ff0f0f00)
        begin
            $display("Output message failed on f0f0f0ff0f0f00");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_PC1_permutation;

module test_PC2_permutation;
    wire[56:1] in1;
    wire[56:1] in2;
    wire[48:1] out1;
    wire[48:1] out2;
    integer i;

    `include "helper_functions.v"

    // Input message of 55555555555555
    // Output message of 9153E54319BD
    assign in1 = 56'h55555555555555;
    assign out1 = PC2_permutation(in1);

    initial
    begin
        if(out1 !== 48'h9153E54319BD)
        begin
            $display("Output message failed on 9153E54319BD");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Input message of 0000000fffffff
    // Output message of 000000ffffff
    assign in2 = 56'h0000000fffffff;
    assign out2 = PC2_permutation(in2);

    initial
    begin
        if(out2 !== 48'h000000ffffff)
        begin
            $display("Output message failed on 000000ffffff");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_PC2_permutation;

module test_SBOX;
    reg[4:1] out;
    wire[6:1] in;

    `include "helper_functions.v"

    assign in = 6'b010101;

    task check_output(input[4:1] test, input[4:1] actual);
        if(test !== actual)
        begin
            $display("Output failed on %d", actual);
            $display("Output was %d", test);
        end
        else
        begin
            $display("Passed");
        end
    endtask

    initial
    begin
        // Select 1
        // Output 12
        out = SBOX(in, 4'd1);
        check_output(out, 4'd12);

        // Select 2
        // Output 1
        out = SBOX(in, 4'd2);
        check_output(out, 4'd1);

        // Select 3
        // Output 5
        out = SBOX(in, 4'd3);
        check_output(out, 4'd5);

        // Select 4
        // Output 2
        out = SBOX(in, 4'd4);
        check_output(out, 4'd2);

        // Select 5
        // Output 15
        out = SBOX(in, 4'd5);
        check_output(out, 4'd15);

        // Select 6
        // Output 13
        out = SBOX(in, 4'd6);
        check_output(out, 4'd13);

        // Select 7
        // Output 5
        out = SBOX(in, 4'd7);
        check_output(out, 4'd5);

        // Select 8
        // Output 6
        out = SBOX(in, 4'd8);
        check_output(out, 4'd6);

    end

endmodule // test_SBOX;


module test_shift_key;
    reg[55:0] out;
    wire[55:0] in;

    `include "helper_functions.v"

    assign in = 56'h80000018000001;

    task check_output(input [55:0] test, input integer shift_value);
        integer actual;
        begin
            if(shift_value == 'd1)
            begin
                actual = 56'h00000030000003;
            end
            else
            begin
                actual = 56'h00000060000006;
            end
            if (test !== actual)
            begin
                $display("Output failed on %h", actual);
                $display("Output was %h", test);
            end
            else
            begin
                $display("Passed");
            end
        end
    endtask

    initial
    begin
        // Round 1
        out = shift_key(in, 'd1);
        check_output(out, 'd1);

        // Round 2
        out = shift_key(in, 'd2);
        check_output(out, 'd1);

        // Round 3
        out = shift_key(in, 'd3);
        check_output(out, 'd2);

        // Round 4
        out = shift_key(in, 'd4);
        check_output(out, 'd2);

        // Round 5
        out = shift_key(in, 'd5);
        check_output(out, 'd2);

        // Round 6
        out = shift_key(in, 'd6);
        check_output(out, 'd2);

        // Round 7
        out = shift_key(in, 'd7);
        check_output(out, 'd2);

        // Round 8
        out = shift_key(in, 'd8);
        check_output(out, 'd2);

        // Round 9
        out = shift_key(in, 'd9);
        check_output(out, 'd1);

        // Round 10
        out = shift_key(in, 'd10);
        check_output(out, 'd2);

        // Round 11
        out = shift_key(in, 'd11);
        check_output(out, 'd2);

        // Round 12
        out = shift_key(in, 'd12);
        check_output(out, 'd2);

        // Round 13
        out = shift_key(in, 'd13);
        check_output(out, 'd2);

        // Round 14
        out = shift_key(in, 'd14);
        check_output(out, 'd2);

        // Round 15
        out = shift_key(in, 'd15);
        check_output(out, 'd2);

        // Round 16
        out = shift_key(in, 'd16);
        check_output(out, 'd1);

    end

endmodule // test_shift_key;

module test_feistel;
    wire[31:0] half_block1;
    wire[31:0] half_block2;
    wire[47:0] subkey1;
    wire[47:0] subkey2;
    reg[31:0] out1;
    reg[31:0] out2;
    integer i;

    `include "helper_functions.v"

    // Half_block 0x0
    // Subkey 0x0
    // Output 0xd8d8dbbc
    assign half_block1 = 32'h0;
    assign subkey1 = 48'h0;

    initial
    begin
        out1 = feistel(half_block1, subkey1);
        if(out1 !== 32'hd8d8dbbc)
        begin
            $display("Output message failed on d8d8dbbc");
            $display("Output was %h", out1);
        end
        else
        begin
            $display("Passed");
        end
    end

    // Half_block 0xffffffff
    // Subkey 0x0
    // Output 0x38dbf9cb
    assign half_block2 = 32'hffffffff;
    assign subkey2 = 48'h0;

    initial
    begin
        out2 = feistel(half_block2, subkey2);
        if(out2 !== 32'h38dbf9cb)
        begin
            $display("Output message failed on 38dbf9cb");
            $display("Output was %h", out2);
        end
        else
        begin
            $display("Passed");
        end
    end
endmodule // test_feistel;
