// Implementation of the Parallel-In-Serial-Out interface.
//
// To change the bit width of the interface, the instantiator simply
// modifies the instance parameter directly:
//
//   module my_mod;
//   ...
//   piso my_piso(data, clk, rst, enable, out);
//   defparam bit_width = 16;
//   my_piso.nbits = bit_width;
//   ...
//   endmodule

module piso (clk, rst, enable, sel, data1, data2, out);
  parameter nbits = 8;  // Default eight bit, for now
  input data1[nbits:1], data2[nbits:1], clk, rst, enable, sel;
  output out;
  wire clk, rst, enable, sel, out; wire [nbits:1] data1, data2;
  reg reg_chain[nbits:1], out_reg;
  integer i;

  // The output should always be the output of the last register
  // in the register chain.
  assign out = out_reg;

  always @ (posedge clk)
  begin
    if (rst) begin

      // TODO: Could make this asynchronous if we care.
      // Synchronous reset. Set each flip-flop's output to zero.
      for (i = 1; i <= nbits; i = i + 1) begin
        reg_chain[i] <= 0;
      end
      out_reg <= 0;

    end else if (enable) begin

      // If the enable bit is set, start clocking out data from the
      // flip-flop chain.
      for (i = 1; i < nbits; i = i + 1) begin
        reg_chain[i + 1] <= reg_chain[i];  
      end
      out_reg <= reg_chain[nbits];

      // Make sure zeros get propagated through the chain after the data
      // is clocked out.
      reg_chain[1] <= 0;

    end else begin

      // If the enable bit is not set, just bring in input data to the
      // flip-flop.
      for (i = 1; i <= nbits; i = i + 1) begin
        if (sel) begin
          reg_chain[i] <= data2[i];
        end else begin
          reg_chain[i] <= data1[i];
        end
      end

      // Prevent oscillating output when the chain is not enabled.
      out_reg <= 0;

    end
  end
endmodule