Waveform Test Files
=====================

The .wlf files in this folder are saved waveforms from ModelSim test bench runs.
To open:

* `vsim -view <filename>`
* Add all the signals to the wave to view the waveforms.
