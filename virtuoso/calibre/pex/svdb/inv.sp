* SPICE NETLIST
***************************************

.SUBCKT crtmom PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT crtmom_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT cfmom PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT cfmom_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT cfmom_mx PLUS1 MINUS1 PLUS2 MINUS2 BULK
.ENDS
***************************************
.SUBCKT efuse PLUS MINUS
.ENDS
***************************************
.SUBCKT hvpwdnwhvnw_dio_hvpw_3t PLUS MINUS SUB
.ENDS
***************************************
.SUBCKT lcesd1_rf PLUS MINUS
.ENDS
***************************************
.SUBCKT lcesd2_rf PLUS MINUS
.ENDS
***************************************
.SUBCKT lowcpad_rf APAD AVSS
.ENDS
***************************************
.SUBCKT lvpwdnwhvnw_dio_hvpw_3t PLUS MINUS SUB
.ENDS
***************************************
.SUBCKT mimcap_1p0_sin TOP BOT
.ENDS
***************************************
.SUBCKT mimcap_1p0_sin_3t PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_1p0_sin_hl TOP BOT
.ENDS
***************************************
.SUBCKT mimcap_1p0_sin_hl_3t PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_1p5_sin TOP BOT
.ENDS
***************************************
.SUBCKT mimcap_1p5_sin_3t PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_2p0_shield PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_2p0_sin TOP BOT
.ENDS
***************************************
.SUBCKT mimcap_2p0_sin_3ds TOP BOTTOM
.ENDS
***************************************
.SUBCKT mimcap_2p0_sin_3ds_3t TOP BOTTOM GNODE
.ENDS
***************************************
.SUBCKT mimcap_2p0_sin_3t PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_2p0_wos PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_4p0_sin_3dshs TOP BOTTOM
.ENDS
***************************************
.SUBCKT mimcap_4p0_sin_3dshs_3t TOP BOTTOM GNODE
.ENDS
***************************************
.SUBCKT mimcap_shield PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT mimcap_wos PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT moscap_rf33 PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT moscap_rf33_nw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT moscap_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT moscap_rf_nw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT nch_hv5_5vnw_ac D G S B
.ENDS
***************************************
.SUBCKT LDDN D G S B
.ENDS
***************************************
.SUBCKT nch_hva_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hva_ndd_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvau_ndd_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvi_mac D G BS SUB
.ENDS
***************************************
.SUBCKT nch_hviah_ndd_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvish_nbl_mac D G S B NBL_ISO SUB
.ENDS
***************************************
.SUBCKT nch_hvish_ndd_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvisl_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvnw_6t D G S B HVNW_ISO SUB
.ENDS
***************************************
.SUBCKT nch_hvs_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvs_ndd_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvsl_mac D G S B
.ENDS
***************************************
.SUBCKT nch_hvsu_ndd_mac D G S B
.ENDS
***************************************
.SUBCKT ndio_hia_rf PLUS MINUS
.ENDS
***************************************
.SUBCKT ndio_sbd_mac PLUS MINUS
.ENDS
***************************************
.SUBCKT ndld24_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT ndld40_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT nhvpwdnw10_4t C B E SUB
.ENDS
***************************************
.SUBCKT nhvpwdnw5_4t C B E SUB
.ENDS
***************************************
.SUBCKT nld12_g12_mac D G BS SUB
.ENDS
***************************************
.SUBCKT nld12_g2_mac D G BS SUB
.ENDS
***************************************
.SUBCKT nld14_g12_mac D G BS SUB
.ENDS
***************************************
.SUBCKT nld14_g2_mac D G BS SUB
.ENDS
***************************************
.SUBCKT nld18_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT nld24_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT nld32_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT nld40_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT nld60_g5_ac D G BS SUB
.ENDS
***************************************
.SUBCKT nmos_rf D G S B
.ENDS
***************************************
.SUBCKT nmos_rf33 D G S B
.ENDS
***************************************
.SUBCKT nmos_rf33_6t D G S B NG PG
.ENDS
***************************************
.SUBCKT nmos_rf_6t D G S B NG PG
.ENDS
***************************************
.SUBCKT nmoscap PLUS MINUS
.ENDS
***************************************
.SUBCKT nmoscap_33 PLUS MINUS
.ENDS
***************************************
.SUBCKT nmoscap_33_mis PLUS MINUS
.ENDS
***************************************
.SUBCKT nmoscap_mis PLUS MINUS
.ENDS
***************************************
.SUBCKT nvmrpodrpo_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT LDDP D G S B
.ENDS
***************************************
.SUBCKT pa18_g5_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pa24_g5_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pa32_g5_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pa40_g5_full_soa_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pa40_g5_low_ron_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pa50_g5_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pa60_g5_full_soa_ac D G S B SUB
.ENDS
***************************************
.SUBCKT pch_5t D G S B SUB
.ENDS
***************************************
.SUBCKT pch_hva_mac D G S B SUB
.ENDS
***************************************
.SUBCKT pch_hva_pdd_mac D G S B
.ENDS
***************************************
.SUBCKT pch_hvau_pdd_mac D G S B
.ENDS
***************************************
.SUBCKT pch_hvs_mac D G S B SUB
.ENDS
***************************************
.SUBCKT pch_hvs_pdd_mac D G S B
.ENDS
***************************************
.SUBCKT pch_hvsl_mac D G S B
.ENDS
***************************************
.SUBCKT pch_hvsu_pdd_mac D G S B
.ENDS
***************************************
.SUBCKT pdio_hia_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT phvnw_dio_hvpw_3t PLUS MINUS SUB
.ENDS
***************************************
.SUBCKT pmos_rf D G S B
.ENDS
***************************************
.SUBCKT pmos_rf33 D G S B
.ENDS
***************************************
.SUBCKT pmos_rf33_5t D G S B PG
.ENDS
***************************************
.SUBCKT pmos_rf33_nw D G S B
.ENDS
***************************************
.SUBCKT pmos_rf33_nw_5t D G S B PG
.ENDS
***************************************
.SUBCKT pmos_rf_5t D G S B PG
.ENDS
***************************************
.SUBCKT pmos_rf_nw D G S B
.ENDS
***************************************
.SUBCKT pmos_rf_nw_5t D G S B PG
.ENDS
***************************************
.SUBCKT rhim PLUS MINUS
.ENDS
***************************************
.SUBCKT rhvnw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rhvnw_60 PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rhvnwhvpw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rhvpw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rhvpw_60 PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rldpwod PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rldpwsti PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rnddhvpw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rnod_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnodrpo_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnodw_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnpo1_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnpo1rpo_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnpo1w_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnwod_ell PLUS MINUS
.ENDS
***************************************
.SUBCKT rnwod_ell_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnwod_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnwod_ull PLUS MINUS
.ENDS
***************************************
.SUBCKT rnwod_ull_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnwsti_ell PLUS MINUS
.ENDS
***************************************
.SUBCKT rnwsti_ell_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnwsti_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rnwsti_ull PLUS MINUS
.ENDS
***************************************
.SUBCKT rnwsti_ull_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpbody PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rpddhvnw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rpod_ell PLUS MINUS
.ENDS
***************************************
.SUBCKT rpod_ell_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpod_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpod_ull PLUS MINUS
.ENDS
***************************************
.SUBCKT rpod_ull_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpodrpo_ell PLUS MINUS
.ENDS
***************************************
.SUBCKT rpodrpo_ell_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpodrpo_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpodrpo_ull PLUS MINUS
.ENDS
***************************************
.SUBCKT rpodrpo_ull_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpodw_ell PLUS MINUS
.ENDS
***************************************
.SUBCKT rpodw_ell_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpodw_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rpodw_ull PLUS MINUS
.ENDS
***************************************
.SUBCKT rpodw_ull_m PLUS MINUS B
.ENDS
***************************************
.SUBCKT rppo1_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rppo1rpo_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rppo1w_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rppolyhri3k PLUS MINUS
.ENDS
***************************************
.SUBCKT rppolyhri3k_dis PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rppolyhri_dis PLUS MINUS B
.ENDS
***************************************
.SUBCKT rppolyhri_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rppolyl_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rppolys_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT rppolywo_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT sbd_dio_60 PLUS MINUS
.ENDS
***************************************
.SUBCKT sbd_rf PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT sbd_rf_nw PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT spiral_std_40k PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT spiral_std_mu_x_40k PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT spiral_sym_40k PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT spiral_sym_ct_40k PLUS MINUS BULK CTAP
.ENDS
***************************************
.SUBCKT spiral_sym_ct_mu_x_40k PLUS MINUS BULK CTAP
.ENDS
***************************************
.SUBCKT spiral_sym_mu_x_40k PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT xjvar_nr36 PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT xjvar_w40 PLUS MINUS BULK
.ENDS
***************************************
.SUBCKT zd_dio_4t PLUS MINUS NBL_ISO SUB
.ENDS
***************************************
.SUBCKT inv in vdd gnd out
** N=4 EP=4 IP=0 FDC=2
* PORT in in 1220 -2710 metal1
* PORT vdd vdd 1790 -660 metal1
* PORT gnd gnd 1800 -4470 metal1
* PORT out out 1915 -2515 metal1
M0 out in gnd gnd N L=1.8e-07 W=2.2e-07 AD=1.984e-13 AS=1.984e-13 PD=1.88e-06 PS=1.88e-06 NRD=4.09917 NRS=4.09917 sa=5.2e-07 sb=5.2e-07 sca=3.38407 scb=0.000134123 scc=7.68663e-09 $X=1410 $Y=-3870 $D=3
M1 out in vdd vdd P L=1.8e-07 W=4.4e-07 AD=2.112e-13 AS=2.112e-13 PD=1.84e-06 PS=1.84e-06 NRD=1.09091 NRS=1.09091 sa=4.8e-07 sb=4.8e-07 sca=5.18319 scb=0.00170006 scc=1.00457e-05 $X=1410 $Y=-1840 $D=39
.ENDS
***************************************
