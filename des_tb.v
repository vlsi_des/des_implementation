`timescale 1ns / 1ps

`include "des.v"

module test_des_encrypt;
    parameter size = 1000; // Size of plaintext/ciphertext

    reg [63:0] plaintext_reg[0:size-1];
    reg [63:0] ciphertext_reg[0:size-1];
    reg [63:0] plaintext;
    wire [63:0] ciphertext;
    reg [63:0] key;
    integer plaintext_file;
    integer ciphertext_file;
    integer key_file;

    reg clk_reg;
    wire clk;
    assign clk = clk_reg;

    integer i; // Index of plaintext
    integer j; // Index of ciphertext
    integer count; // Clock count between file reads
    integer passed; // 0 if failed, 1 if passed

    des_encrypt des_encrypt_test(clk, plaintext, key, ciphertext);

    initial
    begin
        clk_reg = 0;
        plaintext_file = $fopen("plaintext.bin", "r");
        $fread(plaintext_reg, plaintext_file);
        ciphertext_file = $fopen("ciphertext.bin", "r");
        $fread(ciphertext_reg, ciphertext_file);
        key_file = $fopen("key.bin", "r");
        $fread(key, key_file);
        count = 0;
        i = 0;
        j = 0;
        passed = 1;
    end

    always
    begin
        #1 clk_reg = !clk_reg;
        count = count + 1;
    end

    always@(count)
    begin
        if (count >= 20)
        begin
            count = 0;
            plaintext = plaintext_reg[i];
            i = (i + 1) % size;
            if (i == 0) // Cycled through all inputs
            begin
                if (passed)
                begin
                    $display("Passed");
                end
            end
        end
    end

    always@(ciphertext)
    begin
        if (ciphertext !== ciphertext_reg[j])
        begin
            $display("Failed: %h", plaintext_reg[j]);
            $display("Expected: %h", ciphertext_reg[j]);
            $display("Actual:   %h", ciphertext);
            passed = 0;
        end
        j = (j + 1) % size;
    end

endmodule // test_des;

module test_des_decrypt;
    parameter size = 1000; // Size of plaintext/ciphertext

    reg [63:0] plaintext_reg[0:size-1];
    reg [63:0] ciphertext_reg[0:size-1];
    wire [63:0] plaintext;
    reg [63:0] ciphertext;
    reg [63:0] key;
    integer plaintext_file;
    integer ciphertext_file;
    integer key_file;

    reg clk_reg;
    wire clk;
    assign clk = clk_reg;

    integer i; // Index of ciphertext
    integer j; // Index of plaintext
    integer count; // Clock count between file reads
    integer passed; // 0 if failed, 1 if passed

    des_decrypt des_decrypt_test(clk, ciphertext, key, plaintext);

    initial
    begin
        clk_reg = 0;
        plaintext_file = $fopen("plaintext.bin", "r");
        $fread(plaintext_reg, plaintext_file);
        ciphertext_file = $fopen("ciphertext.bin", "r");
        $fread(ciphertext_reg, ciphertext_file);
        key_file = $fopen("key.bin", "r");
        $fread(key, key_file);
        count = 0;
        i = 0;
        j = 0;
        passed = 1;
    end

    always
    begin
        #1 clk_reg = !clk_reg;
        count = count + 1;
    end

    always@(count)
    begin
        if (count >= 20)
        begin
            count = 0;
            ciphertext = ciphertext_reg[i];
            i = (i + 1) % size;
            if (i == 0) // Cycled through all inputs
            begin
                if (passed)
                begin
                    $display("Passed");
                end
            end
        end
    end

    always@(plaintext)
    begin
        if (plaintext !== plaintext_reg[j])
        begin
            $display("Failed: %h", ciphertext_reg[j]);
            $display("Expected: %h", plaintext_reg[j]);
            $display("Actual:   %h", plaintext);
            passed = 0;
        end
        j = (j + 1) % size;
    end

endmodule // test_des;