// Testbench for PISO interface.
`timescale 1ns / 1ps

module piso_tb;
  parameter tb_nbits = 8;
  integer clk_cnt = 0;
  wire [tb_nbits:1] data;
  wire clk, rst, enable, out;
  integer clk_per = 4;

  piso piso_uut (data, clk, rst, enable, out);
  defparam piso_uut.nbits = tb_nbits;

  reg [tb_nbits:1] data_reg;
  reg clk_reg, rst_reg, enable_reg;

  initial
  begin
    data_reg = {(tb_nbits / 2){2'b11}};
    clk_reg = 0;
    rst_reg = 1;
    enable_reg = 0;
    #(clk_per / 2) rst_reg = 0;
    #(3 * clk_per / 2) enable_reg = 1;
  end

  always
  begin
    #1 clk_reg = !clk_reg;
  end

  always
  begin
    #(16 * tb_nbits) rst_reg = !rst_reg;
  end

  always @ (posedge clk)
  begin
    if (enable)
    begin
      clk_cnt = clk_cnt + 1;
      $display("out = %b, clk = %d", out, clk_cnt);
    end
  end

  // Continuous assignments (connections)
  assign data[tb_nbits:1] = data_reg[tb_nbits:1];
  assign clk = clk_reg;
  assign rst = rst_reg;
  assign enable = enable_reg;

endmodule