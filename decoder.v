// Implementation of the command decoder for the DES
// system.
// NOTE: This module is purely combinational.

module decoder(inputs, clk_outs, decrypt);
  input wire [3:1] inputs;

  // The output wires are clock signals for appropriate
  // registers. Only one of these should ever be fired at
  // once. The the input command determines whether the
  // data in the register pertains to 'encrypt' or 'decrypt'
  // mode and whether the data is key data or plaintext data.
  //
  //    0x1 (001) --> Encryption data
  //    0x2 (010) --> Encryption key
  //    0x3 (011) --> Decryption data
  //    0x4 (100) --> Decryption key
  //    else      --> No command
  //
  output wire [4:1] clk_outs;
  output reg decrypt;

  // Command = 0x1 (001)
  // Clock the encryption data buffer.
  assign clk_outs[4] = (inputs == 3'b001);

  // Command = 0x2 (010)
  // Clock the encryption key buffer.
  assign clk_outs[3] = (inputs == 3'b010);

  // Command = 0x3 (011)
  // Clock the decryption data buffer.
  assign clk_outs[2] = (inputs == 3'b011);

  // Command = 0x4 (100)
  // Clock the decryption key buffer.
  assign clk_outs[1] = (inputs == 3'b100);

  always @ (inputs)
  begin

    // Only change the decrypt reg whenever a new command comes in.
    if ((inputs == 3'b100) || (inputs == 3'b011)) begin
      // If the command relates to decryption, set the decrypt flag.
      decrypt <= 1;
    end else if ((inputs == 3'b001) || (inputs == 3'b010)) begin
      decrypt <= 0;
    end else if (decrypt == 1) begin
      decrypt <= 1;
    end else begin 
      decrypt <= 0;
    end

  end

endmodule
