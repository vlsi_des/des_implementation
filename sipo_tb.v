// Testbench for SIPO interface.
`timescale 1ns / 1ps

module sipo_tb;
  wire data, clk, sipo_rst, reg_rst, sipo_enable, reg_enable;
  parameter tb_nbits = 8;
  wire [tb_nbits:1] sipo_out;
  wire [tb_nbits:1] out;
  integer clk_per = 4;
  integer clk_cnt = 0;

  sipo sipo_uut (data, clk, sipo_rst, sipo_enable, sipo_out);
  nbit_reg reg_uut(sipo_out, clk, reg_rst, reg_enable, out);
  defparam sipo_uut.nbits = tb_nbits;
  defparam reg_uut.nbits = tb_nbits;

  reg data_reg, clk_reg, sipo_rst_reg, reg_rst_reg, sipo_enable_reg, reg_enable_reg;

  initial
  begin
    // Begin with the resets triggered so everything is
    // pulled to zero.
    data_reg = 1;
    clk_reg = 1;
    sipo_rst_reg = 1;
    reg_rst_reg = 1;
    sipo_enable_reg = 0;
    reg_enable_reg = 0;

    // Then turn resets off.
    #(clk_per / 2) sipo_rst_reg = 0;
    reg_rst_reg = 0;

    // Enable the SIPO to start clocking in data.
    sipo_enable_reg = 1;

    // After we've filled the whole sipo, clock its
    // contents into the register.
    #(clk_per * tb_nbits) sipo_enable_reg = 0;
    reg_enable_reg = 1;

    // Disable the register (to latch in the data) and
    // clear the SIPO. The data in the register should
    // remain unchanged.
    #clk_per reg_enable_reg = 0;
    sipo_rst_reg = 1;

    // Clear the SIPO reset bit.
    #clk_per sipo_rst_reg = 0;

    // Enable the register again. The SIPO has not yet been
    // re-enabled so this should clock in all zeros,
    // effectively clearing the register.
    #clk_per reg_enable_reg = 1;

    // Latch in the register clear.
    #clk_per reg_enable_reg = 0;
  end

  // Clock block
  always
  begin
    #(clk_per / 2) clk_reg = !clk_reg;
  end

  // Data block. Hold the data the same for two full clock
  // cycles.
  always @ (negedge clk)
  begin
    clk_cnt = clk_cnt + 1;
    if (clk_cnt == 2) begin
      // Change data halfway between negative and positive
      // edges.
      data_reg = !data_reg;
      clk_cnt = 0;
    end
  end

  // Continuous assignments (connections)
  assign data = data_reg;
  assign clk = clk_reg;
  assign sipo_rst = sipo_rst_reg;
  assign reg_rst = reg_rst_reg;
  assign sipo_enable = sipo_enable_reg;
  assign reg_enable = reg_enable_reg;

endmodule