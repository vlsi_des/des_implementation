###################################################################

# Created by write_sdc on Fri Nov 30 15:13:50 2018

###################################################################
set sdc_version 2.0

set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_max_area 0
create_clock [get_ports spi_clk]  -name clk  -period 10  -waveform {0 5}
