#!/bin/bash

function usage
{
    echo "
Usage: $0 <size>
"
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

# Remove previous files
rm -f plaintext.bin ciphertext.bin

# Generate key.bin
low32=$(od -An -td4 -N4 < /dev/urandom)
high32=$(od -An -td4 -N4 < /dev/urandom)
long=$(($low32 + ($high32 << 32) ))
key=$(printf "%016x" $long)
printf "$(echo $key | sed 's/.\{2\}/\\x&/g')" >> temp_keys.bin
dd if=temp_keys.bin bs=8 count=1 > key.bin


# Generate plaintext.bin ciphertext.bin
for i in `seq 1 $1`
do
    low32=$(od -An -td4 -N4 < /dev/urandom)
    high32=$(od -An -td4 -N4 < /dev/urandom)
    long=$(($low32 + ($high32 << 32) ))
    printf "$(printf "%016x" $long | sed 's/.\{2\}/\\x&/g')" > temp_plaintext.bin
    dd if=temp_plaintext.bin bs=8 count=1 >> plaintext.bin

    openssl enc -des-ecb -K $key -in temp_plaintext.bin -out temp_ciphertext.bin
    dd if=temp_ciphertext.bin bs=8 count=1 >> ciphertext.bin
done

rm -f temp_plaintext.bin temp_keys.bin temp_ciphertext.bin