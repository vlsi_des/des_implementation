module pad_out_buffered (out, pad);
  input out;
  output pad;
  wire out_pre, out_pre1, out_pre2, out_buf;

  INVX1 inv0 (.A(out), .Z(out_pre));
  INVX4 inv1 (.A(out_pre), .Z(out_pre1));
  INVX16 inv2 (.A(out_pre1), .Z(out_pre2));
  INVX32 inv3 (.A(out_pre2), .Z(out_buf));
  pad_out pad_out0(.pad(pad), .DataOut(out_buf));
endmodule

module top_pad ( rst, spi_clk, in_en, out_rdy, din, dout );
  input rst, spi_clk, in_en, din;
  output out_rdy, dout;
  wire rst, spi_clk, in_en, din, out_rdy, dout;
  wire rst_pad, spi_clk_pad, in_en_pad, out_rdy_pad, din_pad, dout_pad;


  pad_in pad_in0 (.pad(rst), .DataIn(rst_pad));
  pad_in pad_in1 (.pad(spi_clk), .DataIn(spi_clk_pad));
  pad_in pad_in2 (.pad(in_en), .DataIn(in_en_pad));
  pad_in pad_in3 (.pad(din), .DataIn(din_pad));
  pad_vdd pad_vdd0 ();
  pad_vdd pad_vdd1 ();
  pad_vdd pad_vdd2 ();
  pad_vdd pad_vdd3 ();
  pad_gnd pad_gnd0 ();
  pad_gnd pad_gnd1 ();
  pad_gnd pad_gnd2 ();
  pad_gnd pad_gnd3 ();
  pad_gnd pad_gnd4 ();
  pad_out_buffered pad_out0(.pad(out_rdy), .out(out_rdy_pad));
  pad_out_buffered pad_out1(.pad(dout), .out(dout_pad));
  pad_corner corner0 ();
  pad_corner corner1 ();
  pad_corner corner2 ();
  pad_corner corner3 ();
  
  
  top des_top ( rst_pad, spi_clk_pad, in_en_pad, out_rdy_pad, din_pad, dout_pad );
endmodule
