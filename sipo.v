// Implementation of the Serial-In-Parallel-Out interface.
//
// To change the bit width of the interface, the instantiator simply
// modifies the instance parameter directly:
//
//   module my_mod;
//   ...
//   sipo my_sipo(data, clk, rst, enable, out);
//   defparam bit_width = 16;
//   my_sipo.nbits = bit_width;
//   ...
//   endmodule

module sipo (data, clk, rst, enable, out);
  parameter nbits = 8;  // Default eight bit, for now
  input data, clk, rst, enable;
  output reg [nbits:1] out;
  wire data, clk, rst, enable;
  integer i;

  always @ (posedge clk)
  if (rst) begin
    // Synchronous reset
    out <= 0;
  end else if (enable) begin
    // If the enable bit is set, start clocking in data.

    // Move each bit forward in the register.
    for (i = 1; i < nbits; i = i + 1) begin
      out[i + 1] <= out[i];  
    end

    // Take in the next bit as well.
    out[1] <= data;
  end else begin
  end
endmodule

module nbit_reg(data, clk, rst, enable, out); 
  parameter nbits = 8;  // Default eight bit, for now
  input wire [nbits:1] data;
  input wire clk, rst, enable;
  output reg [nbits:1] out;
  integer i;

  always @ (posedge clk)
  begin
    if (rst) begin

      // Should zero out all the registers.
      for (i = 1; i <= nbits; i = i + 1) begin
        out[i] <= 0;
      end

    end else if (enable) begin

      // Bring new data into the registers.
      for (i = 1; i <= nbits; i = i + 1) begin
        out[i] <= data[i];
      end

    end
    else begin
      // Keep data in the registers.
      for (i = 1; i <= nbits; i = i + 1) begin
        out[i] <= out[i];
      end

    end
  end
endmodule
