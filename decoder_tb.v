// Test bench for the DES system command decoder.
//
`timescale 1ns / 1ps

module decoder_tb;
  wire [3:1] inputs;
  wire [4:1] outputs;
  wire decrypt;
  reg [3:1] inputs_reg;
  integer i;

  decoder decoder_uut(inputs, outputs, decrypt);

  initial
  begin
    // Set everything to zero initially.
    for (i = 1; i <= 3; i = i + 1) begin
      inputs_reg[i] <= 0;
    end

    // Change the command to 001 (should set the MSB high).
    #4 inputs_reg = 3'b001;

    // Change the command to 010 (should set 2nd bit high).
    #4 inputs_reg = 3'b010;

    // Change the command to 011 (should set the 3rd bit high).
    #4 inputs_reg = 3'b011;

    // Change the command to 100 (should set the 2nd bit high).
    #4 inputs_reg = 3'b100;

    // Try some other commands (that aren't defined) to make
    // sure all outputs are low in these cases.
    #4 inputs_reg = 3'b110;
    #4 inputs_reg = 3'b111;
    #4 inputs_reg = 3'b000;

  end

  assign inputs = inputs_reg;

endmodule