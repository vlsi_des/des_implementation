// Implementation of the DES system as a whole.
//

`include "sipo.v"
`include "piso.v"
`include "decoder.v"
`include "des.v"

module top (rst, spi_clk, in_en, out_rdy, din, dout);
  input wire rst, spi_clk, in_en, din;
  output wire  out_rdy, dout;
  parameter in_bits = 67;
  parameter key_bits = 64;
  parameter data_bits = 64;
  parameter cmd_bits = 3;
  parameter num_cmds = 4;

  // ===============================================================
  // FIRST STAGE:
  //    - 72-bit SIPO
  //    - Most significant 8-bits are the command associated with
  //      the data.
  // ===============================================================
  wire [in_bits:1] sipo_out;
  sipo input_sipo(din, spi_clk, rst, !in_en, sipo_out);
  defparam input_sipo.nbits = in_bits;

  // ===============================================================
  // SECOND STAGE:
  //    - 8-bit command register
  //    - Command decoder (for command in register).
  // ===============================================================
  wire [cmd_bits:1] cmd_reg_out;
  wire [num_cmds:1] decoder_outs;
  wire decrypt;
  nbit_reg cmd_reg(sipo_out[in_bits:(in_bits - (cmd_bits - 1))],
                   spi_clk,
                   (rst | (decoder_outs != 0)),
                   in_en,
                   cmd_reg_out);
  defparam cmd_reg.nbits = cmd_bits;
  //    0x1 (001) --> Encryption data --> outputs[4]
  //    0x2 (010) --> Encryption key  --> outputs[3]
  //    0x3 (011) --> Decryption data --> outputs[2]
  //    0x4 (100) --> Decryption key  --> outputs[1]
  //    else      --> No command
  decoder cmd_decoder(cmd_reg_out, decoder_outs, decrypt);

  // ===============================================================
  // THIRD STAGE:
  //    - 64-bit plain text register
  //    - 64-bit key register
  // ===============================================================
  wire [data_bits:1] data_reg_out;
  wire [key_bits:1] key_reg_out;
  nbit_reg data_reg(sipo_out[(in_bits - cmd_bits):1],
                    ((decoder_outs == 4'd8) || (decoder_outs == 4'd2)),
                    rst,
                    1'b1,
                    data_reg_out);
  nbit_reg key_reg(sipo_out[(in_bits - cmd_bits):1],
                   ((decoder_outs == 4'd4) || (decoder_outs == 4'd1)),
                   rst,
                   1'b1,
                   key_reg_out);
  defparam data_reg.nbits = data_bits;
  defparam key_reg.nbits = key_bits;

  // ===============================================================
  // FOURTH STAGE:
  //    - 64-bit DES encryption engine
  // ===============================================================
  wire [data_bits:1] encrypt_out;
  wire [data_bits:1] decrypt_out;
  des_decrypt decryptor(spi_clk,
                        (decrypt == 1),
                        data_reg_out,
                        key_reg_out,
                        decrypt_out);
  des_encrypt encryptor(spi_clk,
                        (decrypt == 0),
                        data_reg_out,
                        key_reg_out,
                        encrypt_out);

  // ===============================================================
  // FIFTH STAGE:
  //    - 64-bit DES cipher text register
  // ===============================================================
  wire des_reg_enable;
  integer des_count;
  integer des_count_enable;
  assign des_reg_enable = (des_count == 17);

  always @ (negedge spi_clk)
  begin
    if (rst) begin
      des_count = 0;
      des_count_enable = 0;
    end else if ((decoder_outs == 4'd8) || (decoder_outs == 4'd2)) begin
      des_count_enable = 1;
      des_count = 0;
    end else if (des_count_enable) begin
      des_count = des_count + 1;
      if (des_reg_enable) begin
        des_count_enable = 0;
      end
      else begin
        des_count_enable = 1;
      end
    end
    else begin
      des_count = 0;
      des_count_enable = 0;
    end
  end

  // ===============================================================
  // SIXTH STAGE:
  //    - 64-bit PISO 
  // ===============================================================
  integer out_rdy_count;
  reg out_rdy_reg;
  assign out_rdy = out_rdy_reg;
  always @ (negedge spi_clk)
  begin
    if (rst) begin
      out_rdy_reg = 0;
      out_rdy_count = 65;
    end else if (des_reg_enable) begin
      out_rdy_reg = 0;
      out_rdy_count = 0;
    end else if (out_rdy_count > 64) begin
      out_rdy_count = 65;
      out_rdy_reg = 0;
    end else begin
      out_rdy_reg = 1;
      out_rdy_count = out_rdy_count + 1;
    end
  end

  piso output_piso(spi_clk,
                    rst,
                    out_rdy,
                    decrypt,
                    encrypt_out,
                    decrypt_out,
                    dout);
  defparam output_piso.nbits = data_bits;
  
endmodule
