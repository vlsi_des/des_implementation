// Test bench for the DES system.
//
`timescale 1ns / 1ps

`include "top.v"

module encrypt_tb;
  parameter size = 1000; // Size of plaintext/ciphertext

  reg spi_clk_reg;
  reg in_en_reg;
  reg out_rdy_reg;
  reg din_reg;
  reg rst_reg;
  reg [1:64] key_reg;
  reg [1:64] plaintext_reg[1:size];
  reg [1:64] ciphertext_reg[1:size];
  wire rst, spi_clk, in_en, out_rdy, din, dout;
  parameter hf_clk_per = 8'd10;
  parameter spi_clk_per = 8'd100;
  parameter des_cycles = 8'd16;
  integer key_file;
  integer plaintext_file;
  integer ciphertext_file;
  integer i;
  integer plaintext_index;
  integer ciphertext_i;
  integer ciphertext_j;
  integer ciphertext_pass;
  integer key_failure_count = 0;
  integer data_failure_count = 0;

  // Test setup.
  assign rst = rst_reg;
  assign spi_clk = spi_clk_reg;
  assign din = din_reg;
  assign in_en = in_en_reg;

  // Full system under test.
  top top_uut(rst, spi_clk, in_en, out_rdy, din, dout);

  initial
  begin
    // Read in the key, plaintext, and ciphertext
    key_file = $fopen("key.bin", "r");
    $fread(key_reg, key_file);
    plaintext_file = $fopen("plaintext.bin", "r");
    $fread(plaintext_reg, plaintext_file);
    ciphertext_file = $fopen("ciphertext.bin", "r");
    $fread(ciphertext_reg, ciphertext_file);
    
    spi_clk_reg = 0;
    din_reg = 1;
    in_en_reg = 0;
    out_rdy_reg = 0;
    plaintext_index = 1;
    ciphertext_i = 1;
    ciphertext_j = 0;
    ciphertext_pass = 1;

    // Turn reset on to clear everything to zeros.
    rst_reg = 1;

    // Undo the reset.
    #spi_clk_per rst_reg = 0;

    // Clock in the command to set the key (3'b010)
    // followed by 64 bits of ones.
    din_reg = 0;
    #spi_clk_per din_reg = 1;
    #spi_clk_per din_reg = 0;
    #spi_clk_per;

    // Set the input enable bit high after the last data bit is set.
    // This should latch in the data to the key register.
    for (i=1; i <= 64; i=i+1)
    begin
      din_reg = key_reg[i];
      #spi_clk_per;
    end
    in_en_reg = 1;

    // Wait for 2 clock cycles to check the key
    #(1 * spi_clk_per);
    if (top_uut.key_reg.out !== key_reg)
    begin
      $display("Failed: key value is %h", top_uut.key_reg.out);
      key_failure_count = 1;
    end
    else
    begin
      $display("Key passed");
    end


    for (plaintext_index=1; plaintext_index<=1000; plaintext_index=plaintext_index+1)
    begin
      // Now set the input enable bit low to start clocking in the
      // command (3'b001) to set a chunk of data.
      #spi_clk_per din_reg = 0;
      in_en_reg = 0;
      #(2 * spi_clk_per) din_reg = 1;
      #spi_clk_per;

      for (i=1; i<=64; i=i+1)
      begin
        din_reg = plaintext_reg[plaintext_index][i];
        #spi_clk_per;
      end
      // Latch in the data.
      in_en_reg = 1;

      // Set the input enable bit low again so that the command doesn't keep
      // getting latched in.
      #(spi_clk_per) in_en_reg = 0;
      $display("Data latched");
    end

  end

  always @ (posedge spi_clk)
  begin
    if (out_rdy)
    begin
      if (ciphertext_j > 0)
      begin
        if (ciphertext_reg[ciphertext_i][ciphertext_j] !== dout)
        begin
          ciphertext_pass = 0;
        end
      end

      ciphertext_j = ciphertext_j + 1;

      if (ciphertext_j > 64)
      begin
        if (ciphertext_pass)
        begin
          $display("Ciphertext passed");
        end
        else
        begin
          $display("Ciphertext failed");
          data_failure_count = data_failure_count + 1;
        end

        ciphertext_pass = 1;
        ciphertext_j = 0;
        ciphertext_i = ciphertext_i + 1;
      end
    end
    if (ciphertext_i == (size + 1))
    begin
      $display("\nDone\nKey Failures: %d\nData Failures: %d",
                key_failure_count,
                data_failure_count);
      ciphertext_i = ciphertext_i + 1;
    end
  end

  // SPI clock (1 MHz)
  always
  begin
    #(spi_clk_per / 2) spi_clk_reg = !spi_clk_reg;
  end
endmodule

module decrypt_tb;
  parameter size = 1000; // Size of plaintext/ciphertext

  reg spi_clk_reg;
  reg in_en_reg;
  reg out_rdy_reg;
  reg din_reg;
  reg rst_reg;
  reg [1:64] key_reg;
  reg [1:64] plaintext_reg[1:size];
  reg [1:64] ciphertext_reg[1:size];
  wire rst, spi_clk, in_en, out_rdy, din, dout;
  parameter hf_clk_per = 8'd10;
  parameter spi_clk_per = 8'd100;
  parameter des_cycles = 8'd16;
  integer key_file;
  integer plaintext_file;
  integer ciphertext_file;
  integer i;
  integer plaintext_index;
  integer ciphertext_i;
  integer ciphertext_j;
  integer ciphertext_pass;
  integer key_failure_count = 0;
  integer data_failure_count = 0;

  // Test setup.
  assign rst = rst_reg;
  assign spi_clk = spi_clk_reg;
  assign din = din_reg;
  assign in_en = in_en_reg;

  // Full system under test.
  top top_uut(rst, spi_clk, in_en, out_rdy, din, dout);

  initial
  begin
    // Read in the key, plaintext, and ciphertext
    key_file = $fopen("key.bin", "r");
    $fread(key_reg, key_file);
    plaintext_file = $fopen("ciphertext.bin", "r");
    $fread(plaintext_reg, plaintext_file);
    ciphertext_file = $fopen("plaintext.bin", "r");
    $fread(ciphertext_reg, ciphertext_file);
    
    spi_clk_reg = 0;
    din_reg = 1;
    in_en_reg = 0;
    out_rdy_reg = 0;
    plaintext_index = 1;
    ciphertext_i = 1;
    ciphertext_j = 0;
    ciphertext_pass = 1;

    // Turn reset on to clear everything to zeros.
    rst_reg = 1;

    // Undo the reset.
    #spi_clk_per rst_reg = 0;

    // Clock in the command to set the key (3'b100)
    // followed by 64 bits of ones.
    din_reg = 1;
    #spi_clk_per din_reg = 0;
    #spi_clk_per din_reg = 0;
    #spi_clk_per;

    // Set the input enable bit high after the last data bit is set.
    // This should latch in the data to the key register.
    for (i=1; i <= 64; i=i+1)
    begin
      din_reg = key_reg[i];
      #spi_clk_per;
    end
    in_en_reg = 1;

    // Wait for 2 clock cycles to check the key
    #(1 * spi_clk_per);
    if (top_uut.key_reg.out !== key_reg)
    begin
      $display("Failed: key value is %h", top_uut.key_reg.out);
      key_failure_count = 1;
    end
    else
    begin
      $display("Key passed");
    end


    for (plaintext_index=1; plaintext_index<=1000; plaintext_index=plaintext_index+1)
    begin
      // Now set the input enable bit low to start clocking in the
      // command (3'b011) to set a chunk of data.
      #spi_clk_per din_reg = 0;
      in_en_reg = 0;
      #spi_clk_per din_reg = 1;
      #(2 * spi_clk_per);

      for (i=1; i<=64; i=i+1)
      begin
        din_reg = plaintext_reg[plaintext_index][i];
        #spi_clk_per;
      end
      // Latch in the data.
      in_en_reg = 1;

      // Set the input enable bit low again so that the command doesn't keep
      // getting latched in.
      #(spi_clk_per) in_en_reg = 0;
      $display("Data latched");
    end

  end

  always @ (posedge spi_clk)
  begin
    if (out_rdy)
    begin
      if (ciphertext_j > 0)
      begin
        if (ciphertext_reg[ciphertext_i][ciphertext_j] !== dout)
        begin
          ciphertext_pass = 0;
        end
      end

      ciphertext_j = ciphertext_j + 1;

      if (ciphertext_j > 64)
      begin
        if (ciphertext_pass)
        begin
          $display("Plaintext passed");
        end
        else
        begin
          $display("Plaintext failed");
          data_failure_count = data_failure_count + 1;
        end

        ciphertext_pass = 1;
        ciphertext_j = 0;
        ciphertext_i = ciphertext_i + 1;
      end
    end
    if (ciphertext_i == (size + 1))
    begin
      $display("\nDone\nKey Failures: %d\nData Failures: %d",
                key_failure_count,
                data_failure_count);
      ciphertext_i = ciphertext_i + 1;
    end
  end

  // SPI clock (1 MHz)
  always
  begin
    #(spi_clk_per / 2) spi_clk_reg = !spi_clk_reg;
  end
endmodule