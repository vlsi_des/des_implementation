Simulation
===============================================================================
: DES_encrypt
: DES_decrypt

* In linux terminal, run `./generate_test_data.sh 1000`
* In modelsim, load the testbench and type `run 21us`
* You will see "Passed" once the testbench has tested all 1000 plaintext/ciphertext pairs

: initial permutation
: final permutation
: PC1 permutation
: PC2 permutation
: permutation
: SBOX
: feistel
: expansion
: shift_key

* In modelsim, load the testbench and type `run`
* You will see "Passed" if the two tests succeeded


Synthesis
===============================================================================
```
cd synthesis
design_vision-tsmc
```

```
source synthesis.tcl
```

Update Mapped Design
===============================================================================
```
cat top_pad.v >> HDL/GATE/top_mapped.v
```


Place and Route
===============================================================================

```
cd place_and_route
innovus-tsmc
```

```
source SCRIPTS/place_route.tcl
```
Open the GUI and do the following:
File -> Import Design -> Load.. -> CONF/design.globals -> OK
```
do_steps 1 14
```
